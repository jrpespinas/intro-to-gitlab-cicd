Gitlab CI/CD Basics
===

## Overview

Inspect the [`.gitlab-ci.yml`](https://gitlab.com/jrpespinas/intro-to-gitlab-cicd/-/blob/main/.gitlab-ci.yml?ref_type=heads) to follow along this overview

### Keys

1. `stages` - Stages contain a group of job that run in parallel. A job from the next stage will not execute if the jobs of the current stage are still running.<br /><br />By default, Gitlab comes with three stages `test`, `build`, and `deploy`.However, we can define our own using the `stages` key.

2. `variables` - Key to store global variables.<br /><br />Note: we can also define job-level variables.

3. `[jobs]` - Jobs are the fundamental elements of Gitlab CI/CD. We have define our own jobs for this repository namely, `run_tests`, `flake8`, and `build_image`.<br />
    - `[jobs].stage` - assigns a job in a stage
    - `[jobs].image` - the environment (operating system, docker image) to run our specific job
    - `[jobs].before_script` - ideal for setting up the necessary dependencies before running the ideal script associated with the job
    - `[jobs].script` - the script which is triggered to perform the job
    - `[jobs].needs` - defines the job (usually from a previous stage) which needs to succeed in order to run this job.
    - `[jobs].services` - provide additional features to the image, it can be mysql database, redis, or Docker in Docker.